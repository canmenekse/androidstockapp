package com.cmenekse.com.stockapplication.StockDetails;

/**
 * Created by canmenekse on 11/04/16.
 */
public class StockListItem
{
    private String Title;
    private String Value;
    private double numericVal;
    private String image;

    public StockListItem(String title, String value)
    {
        Title = title;
        Value = value;
        image="";
    }

    public StockListItem(String title, String value,String image)
    {
        Title = title;
        Value = value;
        this.image=image;
    }



    public String getTitle()
    {
        return Title;
    }

    public void setTitle(String title)
    {
        Title = title;
    }

    public String getValue()
    {
        return Value;
    }

    public void setValue(String value)
    {
        Value = value;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public double getNumericVal()
    {
        return numericVal;
    }

    public void setNumericVal(double numericVal)
    {
        this.numericVal = numericVal;
    }





    public StockListItem()
    {
        numericVal =-7.77;
    }
}
