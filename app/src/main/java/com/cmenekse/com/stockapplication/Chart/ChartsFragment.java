package com.cmenekse.com.stockapplication.Chart;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.cmenekse.com.stockapplication.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChartsFragment extends Fragment
{

    private String symbol;
    private WebView webview;
    public final static boolean enableDebug = false;
    private boolean viewIsAvailable=false;



    public ChartsFragment()
    {
        // Required empty public constructor
    }





    public void displayExceptions(final Exception e,final String methodname,final String extradata)
    {
        if(this.enableDebug)
        {


            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable()
            {
                @Override
                public void run()
                {

                    Toast.makeText(getActivity(),
                            "Exception thrown METHODNAME:" + methodname + " //////// " + e.getMessage().toString(),
                            Toast.LENGTH_LONG).show();

                    Toast.makeText(getActivity(),
                            "EXTRADATA: "  + extradata ,
                            Toast.LENGTH_LONG).show();
                }
            });


        }
    }
    public void displayCrucialException(final String data)
    {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                Toast.makeText(getActivity(),
                        "Exception,Data returned: " + data.toString(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public void displayToastOnUIThread(final String str)
    {

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                Toast.makeText(getActivity(), str, Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void displayToastOnUIThreadDebugOnly(final String str)
    {
        if(this.enableDebug)
        {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    Toast.makeText(getActivity(), str, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_charts, container, false);
    }




    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser)
        {
            String str = "CHARTS Fragment Makes request";
            //displayToastOnUIThreadDebugOnly(str);


            if(getView()!=null)
            {
                WebView webView = (WebView)getView().findViewById(R.id.webView);
                if(this.webview==null)
                {
                    this.webview = webView;
                }
                loadWebPage();

                this.viewIsAvailable=true;


            }
            else
            {
                this.viewIsAvailable=false;
            }

        }

    }

    public void loadWebPage()
    {
        displayToastOnUIThreadDebugOnly("Loading WebPage");
        WebSettings webSettings =this.webview.getSettings();
        webSettings.setAllowUniversalAccessFromFileURLs(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccessFromFileURLs(true);
        webview.setWebViewClient(new WebViewClient(){
            public void onPageFinished(WebView view, String url){
                webview.loadUrl("javascript:getChartsData('" + symbol + "')");
            }});

        webview.loadUrl("file:///android_asset/chart.html");
    }



    @Override
    public void onViewCreated(View view , Bundle savedInstanceState)
    {
        Bundle bundle =this.getArguments();
        symbol =bundle.getString("symbol");
        WebView webView = (WebView)getView().findViewById(R.id.webView);
        if(this.webview==null)
        {
            this.webview = webView;
        }

        if(!this.viewIsAvailable)
        {

            //displayToastOnUIThreadDebugOnly(" CHART Onview Created makes a request");
            loadWebPage();
        }

    }

}
