package com.cmenekse.com.stockapplication;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cmenekse.com.stockapplication.StockDetails.Stock;

/**
 * Created by canmenekse on 15/04/16.
 */
public class StockFvAdapter extends ArrayAdapter<Stock>
{
    public StockFvAdapter(Context context)
    {

        super(context,0);
    }


    public static class ViewHolder
    {
        public TextView symbol;
        public TextView name;
        public TextView lastPrice;
        public TextView changePercent;
        public TextView marketCap;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        ViewHolder viewHolder;
        View view = convertView;
        //That means we need to create a new view, we are not going to recycle the old view
        if (view == null )
        {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.main_activity_stock_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.FavLst_textViewName);
            viewHolder.symbol = (TextView) view.findViewById(R.id.FavLst_textViewSymbol);
            viewHolder.marketCap = (TextView) view.findViewById(R.id.FavLst_textViewMarketCap);
            viewHolder.lastPrice = (TextView) view.findViewById(R.id.FavLst_textViewLastPrice);
            viewHolder.changePercent = (TextView) view.findViewById(R.id.FavLst_textViewChangePercent);
            view.setTag(viewHolder);

        }

        else
        {
            viewHolder = (ViewHolder) view.getTag();
        }
        //After we make sure we have a valid viewholder we can now populate it's fields
        viewHolder.name.setText(getItem(position).getName());
        viewHolder.symbol.setText(getItem(position).getSymbol());

        viewHolder.lastPrice.setText("$ "+decimalConversion(getItem(position).getLastPrice()));

        if(getItem(position).getChangePercent()>0)
        {
            viewHolder.changePercent.setText("+" +decimalConversion(getItem(position).getChangePercent())+"%");
            viewHolder.changePercent.setBackgroundColor(Color.GREEN);
        }
        else if (getItem(position).getChangePercent()<0)
        {
            viewHolder.changePercent.setText(decimalConversion(getItem(position).getChangePercent())+"%");
            viewHolder.changePercent.setBackgroundColor(Color.RED);
        }

        String marketCapVal="";
        double marketCapValue = getItem(position).getMarketCap();
        marketCapVal = hugeNumConverter(marketCapValue);
        viewHolder.marketCap.setText("Market Cap: "+marketCapVal);


        return view;


    }

    public String decimalConversion(double d)
    {
        return String.format("%.2f",d );
    }

    public String hugeNumConverter(double hugeNum)
    {
        String val = "";

        double updatedval = hugeNum / 1000000000;
        double divided = Double.valueOf(decimalConversion(updatedval));
        if (divided<1.00)
        {
            updatedval = hugeNum / 1000000;
            val = decimalConversion(updatedval) + " Million";


        } else
        {
            val = decimalConversion(updatedval) + " Billion";
        }
        return val;
    }



}
