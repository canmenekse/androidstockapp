package com.cmenekse.com.stockapplication.News;

import android.content.Context;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cmenekse.com.stockapplication.R;

/**
 * Created by canmenekse on 17/04/16.
 */
public class NewsAdapter extends ArrayAdapter<NewsItem>
{
    public NewsAdapter(Context context)
    {
        super(context,0);
    }

    public static class ViewHolder
    {
        public TextView titleTextView;
        public TextView descriptionTextView;
        public TextView dateTextView;
        public TextView publisherTextView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        ViewHolder viewHolder;
        View view = convertView;
        //That means we need to create a new view, we are not going to recycle the old view
        if (view == null )
        {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.news_item_row, null);
            viewHolder = new ViewHolder();
            viewHolder.titleTextView=(TextView)view.findViewById(R.id.news_title);
            viewHolder.descriptionTextView=(TextView)view.findViewById(R.id.news_description);
            viewHolder.publisherTextView=(TextView)view.findViewById(R.id.news_publisher);
            viewHolder.dateTextView=(TextView)view.findViewById(R.id.news_date);
            view.setTag(viewHolder);

        }

        else
        {
            viewHolder = (ViewHolder) view.getTag();
        }

        NewsItem news =  getItem(position);

        viewHolder.titleTextView.setClickable(true);
        viewHolder.titleTextView.setMovementMethod(LinkMovementMethod.getInstance());

        viewHolder.titleTextView.setText(Html.fromHtml(news.getAnchoredTitle()));


        viewHolder.descriptionTextView.setText(news.getDescription());
        viewHolder.publisherTextView.setText(news.getPublisher());
        viewHolder.dateTextView.setText(news.getDate());



        return view;


    }


}
