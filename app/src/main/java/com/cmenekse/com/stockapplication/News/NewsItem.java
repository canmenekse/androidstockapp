package com.cmenekse.com.stockapplication.News;

/**
 * Created by canmenekse on 17/04/16.
 */
public class NewsItem
{

    private String Title;
    private String Description;
    private String Source;
    private String Date;
    private String Url;
    private String anchoredTitle;



    public NewsItem()
    {

    }

    public String getTitle()
    {
        return Title;
    }

    public String getSource()
    {
        return Source;
    }

    public void setSource(String source)
    {
        Source = source;
    }

    public String getUrl()
    {
        return Url;
    }

    public void setUrl(String url)
    {
        Url = url;
    }

    public void setTitle(String title)
    {
        this.Title = title;
    }

    public String getDescription()
    {
        return Description;
    }

    public void setDescription(String description)
    {
        this.Description = description;
    }

    public String getPublisher()
    {
        return Source;
    }

    public void setPublisher(String publisher)
    {
        this.Source = publisher;
    }

    public String getDate()
    {
        return Date;
    }

    public void setDate(String date)
    {
        this.Date = date;
    }

    public String getAnchoredTitle()
    {
        return anchoredTitle;
    }

    public void setAnchoredTitle(String anchoredTitle)
    {
        this.anchoredTitle = anchoredTitle;
    }



}
