package com.cmenekse.com.stockapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.cmenekse.com.stockapplication.Chart.ChartsFragment;
import com.cmenekse.com.stockapplication.News.NewsFragment;
import com.cmenekse.com.stockapplication.StockDetails.Stock;
import com.cmenekse.com.stockapplication.StockDetails.StockDetailsFragment;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.danlew.android.joda.JodaTimeAndroid;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ResultActivity extends AppCompatActivity
{

    private final String PREFERENCENAME = "StockAppShared";

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ModifiedViewPager viewPager;
    private String symbol;
    private Activity activity;
    private boolean favBtnSelected = false;
    private CallbackManager callbackManager = null;



    public  boolean sharedprefHasCurrent()
    {
        SharedPreferences sharedPreferences = this.getSharedPreferences(PREFERENCENAME, this.MODE_PRIVATE);
        if(sharedPreferences.contains("CurrentStock"))
        {
            String str= sharedPreferences.getString("CurrentStock","-1");
            if(str.equals("null")||str.equals("-1"))
            {
                return false;
            }
            else
            {
                return  true;
            }
        }
        return false;
    }

    public void addToFavorites()
    {
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCENAME, MODE_PRIVATE);
        if (sharedPreferences.contains("CurrentStock") && sharedprefHasCurrent())
        {
            Gson gson = new Gson();
            //Get the set stock The stock should definitely exist
            String stockJson = sharedPreferences.getString("CurrentStock", "EMPTY");
            Stock stock = gson.fromJson(stockJson, Stock.class);
            if (sharedPreferences.contains("Stocks"))
            {
                String stocks = sharedPreferences.getString("Stocks", "EMPTY");
                Type sType = new TypeToken<List<Stock>>()
                {
                }.getType();
                ArrayList<Stock> stocksList = gson.fromJson(stocks, sType);
                stocksList.add(stock);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Stocks", gson.toJson(stocksList));
                editor.commit();
            } else
            {
                ArrayList<Stock> stocks = new ArrayList<Stock>();
                stocks.add(stock);
                String json = gson.toJson(stocks);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Stocks", json);
                editor.commit();
            }

        }

    }


    public void facebookShare(View v)
    {

        
        callbackManager = CallbackManager.Factory.create();

        final ShareDialog shareDialog = new ShareDialog(this);

        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>()
        {


            @Override
            public void onSuccess(Sharer.Result result)
            {
                if (result.getPostId() != null)
                {

                    Toast.makeText(activity, "You shared the post", Toast.LENGTH_LONG).show();
                } else
                {
                    Toast.makeText(activity, "You canceled sharing the post", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancel()
            {
                Toast.makeText(activity, "You canceled sharing the post", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error)
            {
                Toast.makeText(activity, "Some error occured during the posting", Toast.LENGTH_SHORT).show();
            }
        });

        if (shareDialog.canShow(ShareLinkContent.class))
        {
            ShareLinkContent.Builder builder = new ShareLinkContent.Builder();
            SharedPreferences sharedPreferences = activity.getSharedPreferences(PREFERENCENAME, activity.MODE_PRIVATE);
            Gson gson = new Gson();
            if (sharedPreferences.contains("CurrentStock") && sharedprefHasCurrent())
            {


                Stock stock = gson.fromJson(sharedPreferences.getString("CurrentStock", "-1"), Stock.class);
                String title = "Current Stock Price of " + stock.getName() + ":" + decimalConversion(stock.getLastPrice());
                Uri imageURL = Uri.parse("http://chart.finance.yahoo.com/t?s=" + symbol + "&lang=en-US&width=700&height=200");
                String contentDescription = "Stock Information of " + stock.getName();
                builder.setContentTitle(title);
                builder.setImageUrl(imageURL);
                builder.setContentDescription(contentDescription);
                ShareLinkContent shareLinkContent = builder.build();
                shareDialog.show(shareLinkContent);

            }

        }
    }


    public void removeFromFavorites()
    {
        Gson gson = new Gson();
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCENAME, MODE_PRIVATE);
        if (sharedPreferences.contains("CurrentStock") && sharedprefHasCurrent())
        {
            String currentStockJSON = sharedPreferences.getString("CurrentStock", "EMPTY");
            String stocksJSON = sharedPreferences.getString("Stocks", "EMPTY");

            Type sType = new TypeToken<List<Stock>>()
            {
            }.getType();
            Stock stock = gson.fromJson(currentStockJSON, Stock.class);
            ArrayList<Stock> stocks = gson.fromJson(stocksJSON, sType);

            for (int i = 0; i < stocks.size(); i++)
            {
                if (stocks.get(i).getSymbol().equals(stock.getSymbol()))
                {
                    stocks.remove(i);
                }
            }
            String json = gson.toJson(stocks);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("Stocks", json);
            editor.commit();
        }
    }

    public void favBtnClicked(View v)
    {
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCENAME, MODE_PRIVATE);

        if (!favBtnSelected)
        {
            addToFavorites();
            if (sharedPreferences.contains("CurrentStock") && sharedprefHasCurrent())
            {
                setStatusOfFavBtn(true);
            }
        } else
        {
            removeFromFavorites();
            if (sharedPreferences.contains("CurrentStock") && sharedprefHasCurrent())
            {
                setStatusOfFavBtn(false);
            }
        }
    }


    protected void setStatusOfFavBtn(boolean pressed)
    {
        ImageButton imageButton = (ImageButton) findViewById(R.id.favBtn);
        if (!pressed)
        {
            Drawable drawable = ContextCompat.getDrawable(this, android.R.drawable.btn_star_big_off);
            imageButton.setImageDrawable(drawable);
            favBtnSelected = false;
        } else
        {
            Drawable drawable = ContextCompat.getDrawable(this, android.R.drawable.btn_star_big_on);
            imageButton.setImageDrawable(drawable);
            favBtnSelected = true;
        }

    }

    protected boolean stockIsInFav(String symbol)
    {
        Gson gson = new Gson();
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCENAME, MODE_PRIVATE);
        if (!sharedPreferences.contains("Stocks"))
        {
            return false;
        }
        String stocks = sharedPreferences.getString("Stocks", "EMPTY");
        Type sType = new TypeToken<List<Stock>>()
        {
        }.getType();
        ArrayList<Stock> stocksList = gson.fromJson(stocks, sType);
        for (Stock stock : stocksList)
        {
            if (stock.getSymbol().equals(symbol))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        JodaTimeAndroid.init(this);


        setContentView(R.layout.activity_result);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.colorPrimary)));
        actionBar.setDisplayShowCustomEnabled(true);
        this.activity = this;
        actionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflator.inflate(R.layout.result_activity_action_bar, null);
        actionBar.setCustomView(view);

        viewPager = (ModifiedViewPager) findViewById(R.id.viewpager);


        viewPagerSettings();

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void previousActivity(View v)
    {
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_result, menu);
        return true;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //From the tutorial
    class VAdapter extends FragmentPagerAdapter
    {
        private List<Fragment> fragmentList;
        private List<String> titleList;

        public VAdapter(FragmentManager fragmentManager)
        {
            super(fragmentManager);
            fragmentList = new ArrayList<>();
            titleList = new ArrayList<>();
        }

        @Override
        public Fragment getItem(int position)
        {
            return fragmentList.get(position);
        }

        @Override
        public int getCount()
        {
            return fragmentList.size();
        }

        public void addFragment(String title, Fragment fragment)
        {
            titleList.add(title);
            fragmentList.add(fragment);
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            return titleList.get(position);
        }
    }


    private void viewPagerSettings()
    {
        VAdapter vAdapter = new VAdapter(getSupportFragmentManager());
        String symbol = getIntent().getExtras().getString("symbol");
        this.symbol = symbol;
        Bundle bundle = new Bundle();
        bundle.putString("symbol", symbol);
        //
        if (stockIsInFav(symbol))
        {
            setStatusOfFavBtn(true);
        } else
        {
            setStatusOfFavBtn(false);
        }

        //
        StockDetailsFragment stockdetailsFragment = new StockDetailsFragment();
        stockdetailsFragment.setArguments(bundle);
        NewsFragment newsFragment = new NewsFragment();
        newsFragment.setArguments(bundle);
        ChartsFragment chartsFragment = new ChartsFragment();
        chartsFragment.setArguments(bundle);
        vAdapter.addFragment("CURRENT", stockdetailsFragment);
        vAdapter.addFragment("HISTORICAL", chartsFragment);
        vAdapter.addFragment("NEWS", newsFragment);

        this.viewPager.setAdapter(vAdapter);

    }
    public String decimalConversion(double d)
    {
        return String.format("%.2f",d );
    }
}
