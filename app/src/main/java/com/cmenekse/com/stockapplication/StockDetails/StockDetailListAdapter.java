package com.cmenekse.com.stockapplication.StockDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cmenekse.com.stockapplication.R;

/**
 * Created by canmenekse on 10/04/16.
 */
public class StockDetailListAdapter extends ArrayAdapter<StockListItem>
{




    public StockDetailListAdapter(Context context)
    {

        super(context,0);
    }


    public static class ViewHolder
    {
        public TextView title;
        public ImageView imageView;
        public TextView content;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        View view = convertView;
        //That means we need to create a new view, we are not going to recycle the old view
        if (view == null )
        {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.stocklist_row_item, null);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.list_item_title);
            viewHolder.content = (TextView) view.findViewById(R.id.list_item_content);
             viewHolder.imageView = (ImageView) view.findViewById(R.id.list_image_view);
            view.setTag(viewHolder);

        } else
        {
           viewHolder = (ViewHolder) view.getTag();
        }
        //After we make sure we have a valid viewholder we can now populate it's fields
        viewHolder.title.setText(getItem(position).getTitle());
        viewHolder.content.setText(getItem(position).getValue());
        String imageName = getItem(position).getImage();
        if (imageName.equals(""))
        {
            viewHolder.imageView.setImageDrawable(null);
        }
        else if (imageName.equals("down"))
        {
            viewHolder.imageView.setImageResource(R.drawable.down);
        }

        return view;


    }


}
