package com.cmenekse.com.stockapplication;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by canmenekse on 19/04/16.
 */
public class ModifiedViewPager extends ViewPager
{
    private  boolean enablePaging;

    public ModifiedViewPager(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.enablePaging=false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {

            return false;
    }


    public boolean isEnablePaging()
    {
        return enablePaging;
    }

    public void setEnablePaging(boolean enablePaging)
    {
        this.enablePaging = enablePaging;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        return false;

    }



}
