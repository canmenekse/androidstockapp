package com.cmenekse.com.stockapplication.StockDetails;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cmenekse.com.stockapplication.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import uk.co.senab.photoview.PhotoView;


public class StockDetailsFragment extends Fragment
{
    private final String TAG = "FRAGMENT_STOCK_DETAIL";
    private final String PREFERENCENAME="StockAppShared";


    public static ListView stockDetailList;
    public static ImageView yahooImage;
    public static String symbol;
    private static boolean viewIsAvailable=false;
    public static View lastAddedFooter = null;
    public static View lastAddedHeader =null;
    public final static boolean enableDebug = false;
    public final static boolean enableStressCheck = false;

    public StockDetailsFragment()
    {
        // Required empty public constructor
    }


    public static StockDetailsFragment newInstance()
    {
        StockDetailsFragment fragment = new StockDetailsFragment();
        return fragment;
    }




    public String stressCheck(String response)
    {
        if(enableStressCheck==true)
        {
            Random random=new Random();
            int  rand = Math.abs (random.nextInt());
            if((rand%2)==0)
            {
                return "";
            }
            else
            {
                return response;
            }
        }
        return response;
    }

    public void displayExceptions(final Exception e,final String methodname,final String extradata)
    {
        if(this.enableDebug)
        {


            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable()
            {
                @Override
                public void run()
                {

                    Toast.makeText(getActivity(),
                            "Exception thrown METHODNAME:" + methodname + " //////// " + e.getMessage().toString(),
                            Toast.LENGTH_LONG).show();

                    Toast.makeText(getActivity(),
                            "EXTRADATA: "  + extradata ,
                            Toast.LENGTH_LONG).show();
                }
            });


        }
    }
    public void displayCrucialException(final String data)
    {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                Toast.makeText(getActivity(),
                        "Exception,Data returned: " + data.toString(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public void displayToastOnUIThread(final String str)
    {

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                Toast.makeText(getActivity(), str, Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void displayToastOnUIThreadDebugOnly(final String str)
    {
        if(this.enableDebug)
        {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    Toast.makeText(getActivity(), str, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);


    }

    public void addHeader()
    {


        View headerView =((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.header_stock_details, null, false);
        if(stockDetailList.getHeaderViewsCount()>0)
        {
            stockDetailList.removeHeaderView(lastAddedHeader);
        }
        lastAddedHeader=headerView;


        lastAddedHeader=headerView;
        this.stockDetailList.addHeaderView(lastAddedHeader);

    }



    @Override
    public void setUserVisibleHint(boolean isVisibleToUser)
    {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser)
        {

                String str = "Stock Fragment IS THERE";
                displayToastOnUIThreadDebugOnly(str);
                Log.i(TAG, "Fragment is visible.");
                if(getView()!=null)
                {
                    str="MAKING REQUEST WITH VISIBLE STOCK DETAIL";
                    displayToastOnUIThreadDebugOnly(str);

                viewIsAvailable=true;
                ListView listView =(ListView)getView().findViewById(R.id.stock_list_view);
                this.stockDetailList=listView;

                addHeader();
                makeImageRequest();
                makeQuoteRequest();
                }
                else
                {
                    viewIsAvailable=false;
                }
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_stock_details, container, false);

    }
    @Override
    public void onViewCreated(View view , Bundle savedInstanceState)
    {

        Bundle bundle =this.getArguments();
        symbol =bundle.getString("symbol");
        Log.i(TAG,"Fragment is created");
        ListView listView =(ListView)getView().findViewById(R.id.stock_list_view);
        yahooImage = (ImageView)getView().findViewById(R.id.yahooImage);

        this.stockDetailList=listView;

        if(!viewIsAvailable)
        {
            addHeader();
            makeImageRequest();
            makeQuoteRequest();
        }
    }


    public void makeImageRequest()
    {
        OkHttpClient okHttpClient = new OkHttpClient();
        String imageURL = "http://chart.finance.yahoo.com/t?s="+symbol+"&lang=en-US&width=200&height=100";
        Request request = new Request.Builder().url(imageURL).addHeader("Content-Type","application/png").build();
        Callback callback= new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                displayToastOnUIThread("Image Request Server unresponsive");

            }

            @Override
            public void onResponse(Call call, Response response)
            {
                try
                {
                    processImageRequest(response.body().byteStream());
                }
                catch (IOException e)
                {
                    displayCrucialException("IO Exception in Image Request");
                }
            }
        };
            okHttpClient.newCall(request).enqueue(callback);

    }



    public void removeCurrentStockFromShared()
    {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(PREFERENCENAME, getActivity().MODE_PRIVATE);
        if(sharedPreferences.contains("CurrentStock") && sharedprefHasCurrent() )
        {
            Gson gson = new Gson();
            Stock stock = gson.fromJson(sharedPreferences.getString("CurrentStock", "-1"), Stock.class);
            if(stock.getSymbol()!=symbol)
            {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove("CurrentStock");
                editor.commit();
            }
        }

    }


    public  boolean sharedprefHasCurrent()
    {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(PREFERENCENAME, getActivity().MODE_PRIVATE);
        if(sharedPreferences.contains("CurrentStock"))
        {
           String str= sharedPreferences.getString("CurrentStock","-1");
            if(str.equals("null")||str.equals("-1"))
            {
                return false;
            }
            else
            {
                return  true;
            }
        }
        return false;
    }


    public void makeQuoteRequest()
    {
        OkHttpClient okHttpClient = new OkHttpClient();
        String URL="http://stockcvm79.appspot.com/php/combined.php?requesttype=Quote&symbol="+this.symbol;
        Request request= new Request.Builder().url(URL).build();
        Callback callback = new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                displayToastOnUIThread("Server unresponsive");
                removeCurrentStockFromShared();

            }

            @Override
            public void onResponse(Call call, Response response)
            {

                try
                {
                    processQuoteRequest(response.body().string());
                }
                catch (IOException e)
                {
                    displayCrucialException("IO Exception in Quote Request");
                    removeCurrentStockFromShared();
                }
            }
        };
        okHttpClient.newCall(request).enqueue(callback);


    }


    public void processImageRequest(InputStream inputStream) throws IOException
    {

        try
        {
            Bitmap image = BitmapFactory.decodeStream(inputStream);
            Bitmap resizedbitmap=Bitmap.createBitmap(image, 0,18,200, 82);
            addImageToFooter(resizedbitmap);
            inputStream.close();
        }
        catch (Exception e)
        {
            displayExceptions(e,"processImageRequest","NONE");
        }
    }


    public void addImageToFooter(final Bitmap image)
    {

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                View footerView =((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_stock_details, null, false);

                if(stockDetailList.getFooterViewsCount()>0)
                {
                    stockDetailList.removeFooterView(lastAddedFooter);
                }

                lastAddedFooter=footerView;
                ImageView stockImageView= (ImageView) footerView.findViewById(R.id.yahooImage);

                stockImageView.setImageBitmap(image);
                stockImageView.setVisibility(View.VISIBLE);
                stockImageView.setScaleType(ImageView.ScaleType.FIT_XY);
                stockImageView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {

                        Dialog dialogBuilder = new Dialog(getActivity());
                        dialogBuilder.setContentView(R.layout.image_dialog);
                        PhotoView photoView = (PhotoView)dialogBuilder.findViewById(R.id.dialog_image);
                        photoView.setScaleType(ImageView.ScaleType.FIT_XY);
                        photoView.setImageBitmap(image);
                        dialogBuilder.show();

                    }
                });
                stockDetailList.addFooterView(footerView);
            }
        });


    }


    public String convertDate(String dateStr)
    {
        try
        {

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy, HH:mm:ss");
            Date date = new SimpleDateFormat("E MMM dd HH:mm:ss zzzZZ yyyy").parse(dateStr);
            String convertedDate = dateFormat.format(date);
            return convertedDate;

        }
        catch(Exception e)
        {
            displayExceptions(e,"convertDate",dateStr);
            Log.e(TAG,"Issue with date conversion");
        }
        return dateStr;
    }



    public void processQuoteRequest(String response)
    {

        response=stressCheck(response);
        try
        {
            Gson gson = new Gson();
            Stock stock = gson.fromJson(response, Stock.class);
            SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(PREFERENCENAME, this.getActivity().MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("CurrentStock", gson.toJson(stock));
            editor.commit();
            final StockDetailListAdapter adapter = new StockDetailListAdapter(this.getActivity());
            //Adding the Items to the list
            StockListItem name = new StockListItem("NAME", stock.getName());
            final String stockName = stock.getName();
            adapter.add(name);

            StockListItem symbol = new StockListItem("SYMBOL", stock.getSymbol());
            adapter.add(symbol);

            StockListItem lastPrice = new StockListItem("LASTPRICE", decimalConversion(stock.getLastPrice()));
            adapter.add(lastPrice);

            StringBuilder changeValsb = new StringBuilder();
            String imageType = "";
            changeValsb.append(decimalConversion(stock.getChange()) + "(");

            if (stock.getChangePercent() > 0)
            {
                changeValsb.append("+");
            }

            if(stock.getChangePercent() < 0 || stock.getChange() < 0)
            {
                imageType = "down";
            }
            else if (stock.getChangePercent()>0)
            {
                imageType="up";
            }



            changeValsb.append(decimalConversion(stock.getChangePercent()) + "%)");


            StockListItem change = new StockListItem("CHANGE", changeValsb.toString(), imageType);
            adapter.add(change);


            double marketCapValue = stock.getMarketCap();
            String marketCapVal = hugeNumConverter(marketCapValue);

            StockListItem marketCap = new StockListItem("MARKET CAP", marketCapVal);

            stock.setTimestampLocal(convertDate(stock.getTimestampDefault()));
            StockListItem date = new StockListItem("TIMESTAMP", stock.getTimestampLocal());


            adapter.add(date);
            adapter.add(marketCap);


            StockListItem volume = new StockListItem("VOLUME", String.valueOf(stock.getVolume()));
            adapter.add(volume);

            StringBuilder changeYTDValsb = new StringBuilder();
            imageType = "";
            changeYTDValsb.append(decimalConversion(stock.getChangeYTD()) + "(");
            if (stock.getChangePercentYTD() > 0)
            {
                changeYTDValsb.append("+");

            }

            if(stock.getChangeYTD()< 0 || stock.getChangePercentYTD()< 0 )
            {
                imageType = "down";
            }
            else if ( stock.getChangePercentYTD() > 0)
            {
                imageType="up";
            }
            changeYTDValsb.append(decimalConversion(stock.getChangePercentYTD()));
            changeYTDValsb.append("%)");

            StockListItem changeYTD = new StockListItem("CHANGE YTD", changeYTDValsb.toString(), imageType);
            adapter.add(changeYTD);


            StockListItem high = new StockListItem("HIGH", decimalConversion(stock.getHigh()));
            StockListItem low = new StockListItem("LOW", decimalConversion(stock.getLow()));
            StockListItem open = new StockListItem("OPEN", decimalConversion(stock.getOpen()));

            adapter.add(high);
            adapter.add(low);
            adapter.add(open);
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    stockDetailList.setAdapter(adapter);
                    TextView stockNameTextView = (TextView) getActivity().findViewById(R.id.actionbarStockName);
                    int endingIndex = 15;
                    if (stockName.length() < endingIndex)
                    {
                        endingIndex = stockName.length();
                    }
                    stockNameTextView.setText(stockName.substring(0, endingIndex));
                }
            });
        }

        catch(Exception e)
        {
            displayExceptions(e,"processQuoteRequest",response);
            displayCrucialException(response);
            removeCurrentStockFromShared();

        }



    }

    public String hugeNumConverter(double hugeNum)
    {
        String val = "";

        double updatedval = hugeNum / 1000000000;
        double divided = Double.valueOf(decimalConversion(updatedval));
        if (divided<1.00)
        {
            updatedval = hugeNum / 1000000;
            val = decimalConversion(updatedval) + " Million";


        } else
        {
            val = decimalConversion(updatedval) + " Billion";
        }
        return val;
    }

    public String decimalConversion(double d)
    {
        return String.format("%.2f",d );
    }






}
