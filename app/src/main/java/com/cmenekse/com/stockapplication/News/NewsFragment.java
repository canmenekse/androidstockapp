package com.cmenekse.com.stockapplication.News;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.cmenekse.com.stockapplication.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment
{
    private String symbol="";
    private String TAG="NEWSFRAGMENT";
    private ListView newsList =null;
    private boolean viewIsAvailable=false;
    public final static boolean enableDebug = false;
    public final static boolean enableStressCheck = false;
    public NewsFragment()
    {
        // Required empty public constructor
    }

    public String stressCheck(String response)
    {
        if(enableStressCheck==true)
        {
            Random random=new Random();
            int  rand = Math.abs (random.nextInt());
            if((rand%2)==0)
            {
                return "";
            }
            else
            {
                return response;
            }
        }
        return response;
    }

    public void displayExceptions(final Exception e,final String methodname,final String extradata)
    {
        if(this.enableDebug)
        {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable()
            {
                @Override
                public void run()
                {

                    Toast.makeText(getActivity(),
                            "Exception thrown METHODNAME:" + methodname + " //////// " + e.getMessage().toString(),
                            Toast.LENGTH_LONG).show();

                    Toast.makeText(getActivity(),
                            "EXTRADATA: "  + extradata ,
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }
    public void displayCrucialException(final String data)
    {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                Toast.makeText(getActivity(),
                        "Exception,Data returned: " + data.toString(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public void displayToastOnUIThread(final String str)
    {

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                Toast.makeText(getActivity(), str, Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void displayToastOnUIThreadDebugOnly(final String str)
    {
        if(this.enableDebug)
        {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    Toast.makeText(getActivity(), str, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news, container, false);

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser)
        {
            String str = "NEWS Fragment IS THERE";
            displayToastOnUIThreadDebugOnly(str);
            if(getView()!=null)
            {



                this.viewIsAvailable=true;
                ListView newsList = (ListView) getView().findViewById(R.id.news_list_view);
                this.newsList=newsList;
                makeNewsRequest(symbol);
            }
            else
            {
                this.viewIsAvailable=false;
            }

        }

    }


    @Override
    public void onViewCreated(View view , Bundle savedInstanceState)
    {
        Bundle bundle =this.getArguments();
        symbol =bundle.getString("symbol");
        ListView newsList =(ListView)getView().findViewById(R.id.news_list_view);
        this.newsList=newsList;
        if(!this.viewIsAvailable)
        {
            makeNewsRequest(symbol);

        }
    }



    public void makeNewsRequest(String symbolName)
    {
        displayToastOnUIThreadDebugOnly("MAKING REQUEST WITH NEWS");
        OkHttpClient okHttpClient = new OkHttpClient();
        String URL = "http://stockcvm79.appspot.com/php/combined.php?requesttype=News&symbol=" + symbolName;
        Request request= new Request.Builder().url(URL).build();



        Callback callback = new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                    displayToastOnUIThread("NEWS API server unresponsive");
            }

            @Override
            public void onResponse(Call call, Response response)
            {

                try
                {
                    processNews(response.body().string());
                }
                catch (IOException e)
                {
                    displayExceptions(e,"makeNewsRequest","IO Exception");
                }
            }
        };
        okHttpClient.newCall(request).enqueue(callback);
    }

    public String convertedDate(String dateTmeStr)
    {
        try
        {
            DateTimeFormatter formatter = ISODateTimeFormat.dateTimeParser();
            DateTimeFormatter formatDT = DateTimeFormat.forPattern("dd MMMM yyyy, HH:mm:ss");
            DateTime dt = formatter.parseDateTime(dateTmeStr);
            DateTimeZone timeZone = DateTimeZone.forID("America/Los_Angeles");
            dt.withZoneRetainFields(timeZone);
            String convertedDate = dt.toString(formatDT);
            return convertedDate;
        }
        catch (Exception e)
        {
            displayExceptions(e,"convertedDate in news",dateTmeStr);
        }
        return dateTmeStr;
    }




    public void processNews(String response)
    {
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        try
        {
            JsonObject jsonObject = parser.parse(response).getAsJsonObject();
            JsonObject d = jsonObject.getAsJsonObject("d");
            JsonArray resultsJson = d.getAsJsonArray("results");
            Type sType = new TypeToken<List<NewsItem>>()
            {
            }.getType();
            ArrayList<NewsItem> newsItems = gson.fromJson(resultsJson.toString(), sType);
            final NewsAdapter newsAdapter = new NewsAdapter(this.getActivity());

            for (NewsItem newsItem : newsItems)
            {

                String anchor = "<a href='"+newsItem.getUrl()+"'>" +newsItem.getTitle()+"</a>";
                String date = newsItem.getDate();
                String updatedDate =convertedDate(date);

                newsItem.setDate("Date: " + updatedDate);
                String publisher = newsItem.getPublisher();
                newsItem.setPublisher("Publisher: " + publisher);
                newsItem.setAnchoredTitle(anchor);

                newsAdapter.add(newsItem);
            }
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    newsList.setAdapter(newsAdapter);
                }
            });
        }
        catch (Exception e)
        {
            displayExceptions(e,"processNews",response);
            displayCrucialException(response);
        }
    }


}
