package com.cmenekse.com.stockapplication;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.cmenekse.com.stockapplication.Lookup.LookupWrapper;
import com.cmenekse.com.stockapplication.StockDetails.Stock;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class MainActivity extends ActionBarActivity
{


    public static Activity M_Activity = null;
    private final String TAG = "MainActivity";
    private static boolean disableDropdown = false;
    private final String PREFERENCENAME = "StockAppShared";
    private boolean enableDebug = false;
    public final static boolean enableStressCheck = false;

    private Timer automaticTaskT;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_ACTION_BAR);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //To create the top bar with logo on it
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.colorPrimary)));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        //cleanAllData();
        //We are gonna use this in requester
        M_Activity = this;
        //We set the listeners for the AutocompleteText View
        setupAutoCompleteView();
        setupToggle();

        final DynamicListView activitylistView = (DynamicListView) findViewById(R.id.main_activity_list_view);
        activitylistView.enableSwipeToDismiss(
                new OnDismissCallback()
                {
                    @Override
                    public void onDismiss(@NonNull final ViewGroup listView, @NonNull final int[] reverseSortedPositions)
                    {
                        for (int position : reverseSortedPositions)
                        {
                            StockFvAdapter stockFvAdapter = (StockFvAdapter) activitylistView.getAdapter();
                            Stock stock = stockFvAdapter.getItem(position);
                            String symbol = stock.getSymbol();
                            AlertDialog alertDialog = createAlertDialog(symbol, stockFvAdapter, stock).create();
                            alertDialog.show();
                        }
                    }
                });
    }

    public void displayExceptions(final Exception e, final String methodname, final String extradata)
    {
        if (this.enableDebug)
        {


            Handler handler = new Handler(Looper.getMainLooper());
            final String display = "Exception thrown METHODNAME:" + methodname + " //////// " + e.getMessage().toString();

            handler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    Toast.makeText(M_Activity, display, Toast.LENGTH_LONG).show();
                    Toast.makeText(M_Activity, "EXTRADATA: " + extradata, Toast.LENGTH_LONG).show();
                }
            });


        }
    }


    public String stressCheck(String response)
    {
        if(enableStressCheck==true)
        {
            Random random=new Random();
            int  rand = Math.abs (random.nextInt());
            if((rand%2)==0)
            {
                return "";
            }
            else
            {
                return response;
            }
        }
        return response;
    }

    public void displayToastOnUIThread(final String str)
    {

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                Toast.makeText(M_Activity, str, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void displayCrucialException(final String data)
    {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                Toast.makeText(M_Activity, "Exception, Data returned: " + data.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void displayToastOnUIThreadDebugOnly(final String str)
    {
        if (this.enableDebug)
        {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    Toast.makeText(M_Activity, str, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }




    public void cleanAllData()
    {
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCENAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }


    public void clearAllDataMenu(MenuItem item)
    {
       cleanAllData();
        DynamicListView listView = (DynamicListView) findViewById(R.id.main_activity_list_view);
        if(listView!=null)
        {
            listView.setAdapter(null);
        }
    }

    public void removeFromFavorites(String symbol)
    {
        Gson gson = new Gson();
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCENAME, MODE_PRIVATE);
        String stocksJSON = sharedPreferences.getString("Stocks", "EMPTY");

        Type sType = new TypeToken<List<Stock>>()
        {
        }.getType();
        ArrayList<Stock> stocks = gson.fromJson(stocksJSON, sType);

        for (int i = 0; i < stocks.size(); i++)
        {
            if (stocks.get(i).getSymbol().equals(symbol))
            {
                stocks.remove(i);
            }
        }
        String json = gson.toJson(stocks);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Stocks", json);
        editor.commit();
    }

    public AlertDialog.Builder createAlertDialog(final String symbol, final StockFvAdapter stockFvAdapter, final Stock stock)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete");
        builder.setMessage("Do you want to delete " + symbol + " stock?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                stockFvAdapter.remove(stock);
                stockFvAdapter.notifyDataSetChanged();
                removeFromFavorites(symbol);
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });
        return builder;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        try
        {
            Gson gson = new Gson();
            SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCENAME, MODE_PRIVATE);
            Type sType = new TypeToken<List<Stock>>()
            {
            }.getType();
            DynamicListView listView = (DynamicListView) findViewById(R.id.main_activity_list_view);

            if (sharedPreferences.contains("Stocks"))
            {
                ArrayList<Stock> stocks = gson.fromJson(sharedPreferences.getString("Stocks", "EMPTY"), sType);
                StockFvAdapter stockFvAdapter = new StockFvAdapter(this);
                for (Stock s : stocks)
                {
                    stockFvAdapter.add(s);
                }
                listView.setAdapter(stockFvAdapter);
                addOnclickListenerToListView();
            }
            prepareRefresh(listView);
        }
        catch (Exception e)
        {
            displayExceptions(e, "Main_onResume", "");
        }
    }

    public void prepareRefresh(View v)
    {


        Gson gson = new Gson();
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCENAME, MODE_PRIVATE);
        Type sType = new TypeToken<List<Stock>>()
        {
        }.getType();
        if (sharedPreferences.contains("Stocks"))
        {
            displayToastOnUIThreadDebugOnly("Refreshing Stocks");
            ArrayList<Stock> stocks = gson.fromJson(sharedPreferences.getString("Stocks", "EMPTY"), sType);
            for (Stock s : stocks)
            {
                makeRefresh(s.getSymbol());
            }
        }
    }

    public void makeRefresh(final String symbolname)
    {
        displayToastOnUIThreadDebugOnly("Refresh : " + symbolname);
        OkHttpClient okHttpClient = new OkHttpClient();
        String URL = "http://stockcvm79.appspot.com/php/combined.php?requesttype=Quote&symbol=" + symbolname;
        Request request = new Request.Builder().url(URL).build();
        Callback callback = new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                displayToastOnUIThread("server unresponsive");
            }

            @Override
            public void onResponse(Call call, Response response)
            {
                try
                {
                    processRefreshedData(response.body().string());
                }
                catch (IOException e)
                {
                    displayExceptions(e,"makeRefresh","IO Exception");
                }
            }
        };
        okHttpClient.newCall(request).enqueue(callback);
    }

    public void processRefreshedData(String response)
    {
        response=stressCheck(response);
        try
        {
            DynamicListView listView = (DynamicListView) findViewById(R.id.main_activity_list_view);
            final StockFvAdapter stockFvAdapter = (StockFvAdapter) listView.getAdapter();
            Gson gson = new Gson();
            final Stock stock = gson.fromJson(response, Stock.class);


            Handler handler = new Handler(Looper.getMainLooper());
            updateSharedPrefData(stock);
            handler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    updateItemWithSymbol(stock.getSymbol(), stockFvAdapter, stock.getLastPrice(), stock.getChange(), stock.getChangePercent());
                    addOnclickListenerToListView();
                    stockFvAdapter.notifyDataSetChanged();

                }
            });
        }
        catch (Exception e)
        {
            displayExceptions(e, "Main_processRefreshedData", response);
            displayCrucialException(response);
        }


    }


    public void updateSharedPrefData(Stock stockToUpdate)
    {
        Gson gson = new Gson();
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCENAME, MODE_PRIVATE);
        Type sType = new TypeToken<List<Stock>>()
        {
        }.getType();
        ArrayList<Stock> stocks = gson.fromJson(sharedPreferences.getString("Stocks", "EMPTY"), sType);
        for (Stock s : stocks)
        {
            if (s.getSymbol().equals(stockToUpdate.getSymbol()))
            {
                s.setLastPrice(stockToUpdate.getLastPrice());
                s.setChange(stockToUpdate.getChange());
                s.setChangePercent(stockToUpdate.getChangePercent());

            }
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Stocks", gson.toJson(stocks));
        editor.commit();

    }


    public void clearAutoCompleteTextView(View v)
    {
        AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteSearchStock);
        autoCompleteTextView.setText("");
    }


    public void setupToggle()
    {
        Switch s = (Switch) findViewById(R.id.autoRefreshToggle);
        s.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if (isChecked)
                {

                    automaticTaskT = new Timer();
                    TimerTask asyncTask = new TimerTask()
                    {
                        @Override
                        public void run()
                        {
                            try
                            {
                                prepareRefresh(findViewById(R.id.main_activity_list_view));
                            }
                            catch (Exception e)
                            {

                            }
                        }
                    };
                    automaticTaskT.schedule(asyncTask, 0, 10000);
                } else
                {
                    automaticTaskT.cancel();
                }
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setupAutoCompleteView()
    {
        final AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteSearchStock);
        autoCompleteTextView.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

                String symbolName = s.toString();

                if (!disableDropdown)
                {
                    if (symbolName.length() > 2)
                    {
                        ProgressBar progressBar = (ProgressBar) findViewById(R.id.stockLoadingProgressBar);
                        progressBar.setVisibility(View.VISIBLE);
                        makeLookupRequest(symbolName);
                    } else
                    {
                        autoCompleteTextView.dismissDropDown();
                    }
                } else
                {
                    if (!symbolName.equals(""))
                    {
                        autoCompleteTextView.dismissDropDown();
                        disableDropdown = false;
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable s)
            {
            }
        });
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                autoCompleteTextView.setText("");
                String selectedText = (String) parent.getItemAtPosition(position);
                selectedText = selectedText.substring(0, selectedText.indexOf("-"));
                selectedText = selectedText.trim();
                disableDropdown = true;
                autoCompleteTextView.append(selectedText);


            }
        });

    }

    public AlertDialog createAlertOKDialogWithMessage(String message, Activity activity)
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        dialogBuilder.setMessage(message).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        return alertDialog;
    }


    public void getQuote(View v)
    {
        AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteSearchStock);
        String symbol = autoCompleteTextView.getText().toString();
        if (symbol.trim().equals(""))
        {
            AlertDialog alertDialog = createAlertOKDialogWithMessage("Please enter a Stock Name/Symbol", M_Activity);
            alertDialog.show();

        } else
        {
            makeValidationRequest(symbol);
        }
    }


    private void makeLookupRequest(String symbolName)
    {
        OkHttpClient okHttpClient = new OkHttpClient();
        String URL = "http://stockcvm79.appspot.com/php/combined.php?requesttype=Lookup&symbol=" + symbolName;
        Request request = new Request.Builder().url(URL).build();


        Callback callback = new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                displayToastOnUIThread("Server unresponsive");
            }

            @Override
            public void onResponse(Call call, Response response)
            {
                try
                {
                    processLookup(response.body().string());
                }
                catch (IOException e)
                {
                    displayExceptions(e,"makeLookupRequest","IO Exception");
                }
            }
        };
        okHttpClient.newCall(request).enqueue(callback);
    }


    private void makeValidationRequest(final String symbolName)
    {
        OkHttpClient okHttpClient = new OkHttpClient();

        String URL = "http://stockcvm79.appspot.com/php/combined.php?requesttype=Quote&symbol=" + symbolName;
        Request request = new Request.Builder().url(URL).build();


        Callback callback = new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                displayToastOnUIThread("Server unresponsive");
            }

            @Override
            public void onResponse(Call call, Response response)
            {
                try
                {
                    processValidation(response.body().string());
                }
                catch (IOException e)
                {
                    displayCrucialException("IO Exception in Quote Request");
                }
            }
        };
        okHttpClient.newCall(request).enqueue(callback);
    }


    private boolean isValidStock(JsonObject jsonObject)
    {
        if (jsonObject.get("Status") == null)
        {
            return false;
        }
        if (jsonObject.get("Message") != null)
        {
            return false;
        }
        JsonElement jsonElement = jsonObject.get("Status");
        String status = jsonElement.getAsString();
        if (status.indexOf("Failure") >= 0)
        {
            return false;
        }
        return true;
    }

    private void processValidation(String response)
    {
        response=stressCheck(response);
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        try
        {
            final JsonObject jsonObject = parser.parse(response).getAsJsonObject();
            final Activity currentActivity = this;


            if (!isValidStock(jsonObject))
            {
                Log.i(TAG, "Not validated");
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        AlertDialog alertDialog = createAlertOKDialogWithMessage("Invalid Symbol", M_Activity);
                        alertDialog.show();
                    }
                });

            } else
            {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Intent intent = new Intent(currentActivity, ResultActivity.class);
                        String symbol = jsonObject.get("Symbol").getAsString();
                        intent.putExtra("symbol", symbol);
                        currentActivity.startActivity(intent);
                    }
                });

            }
        }
        catch (Exception e)
        {

            displayExceptions(e, "processValidation", response);
            displayCrucialException(response);
        }
    }


    private void processLookup(String response)
    {
        response=stressCheck(response);
        Gson gson = new Gson();
        List<LookupWrapper> lookupWrappers = null;
        try
        {

            Type lType = new TypeToken<List<LookupWrapper>>()
            {
            }.getType();
            lookupWrappers = (List<LookupWrapper>) gson.fromJson(response, lType);
            ArrayList<String> items = new ArrayList<String>();

            for (LookupWrapper wrapper : lookupWrappers)
            {
                String item = wrapper.getSymbol() + " - " + wrapper.getName() + " - " + wrapper.getExchange();
                items.add(item);

            }
            final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
            final AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) this.findViewById(R.id.autoCompleteSearchStock);
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    autoCompleteTextView.setAdapter(adapter);
                    Log.i(TAG, "Show dropdown called");
                    ProgressBar progressBar = (ProgressBar) findViewById(R.id.stockLoadingProgressBar);
                    progressBar.setVisibility(View.GONE);
                    autoCompleteTextView.showDropDown();

                }
            });


        }
        catch (NullPointerException e)
        {
            Log.e(TAG, e.toString());
            displayExceptions(e, "ProcessLookup_NULLPTR", response);
            displayCrucialException(response);
        }

        catch (JsonParseException e)
        {
            Log.e(TAG, e.toString());
            displayExceptions(e, "ProcessLookup_JSONPARSE", response);
            displayCrucialException(response);

        }
        catch (Exception e)
        {
            Log.e(TAG, e.toString());
            displayExceptions(e, "ProcessLookup_EXCEPTION", response);
            displayCrucialException(response);

        }

    }


    private void updateItemWithSymbol(String symbol, StockFvAdapter adapter, double lastPrice, double change, double changePercent)
    {

        int index = findItemWithSymbol(symbol, adapter);
        if(index!=-1)
        {
            adapter.getItem(index).setLastPrice(lastPrice);
            adapter.getItem(index).setChangePercent(changePercent);
            adapter.getItem(index).setChange(change);
        }
    }

    private int findItemWithSymbol(String symbol, StockFvAdapter adapter)
    {
        for (int i = 0; i < adapter.getCount(); i++)
        {
            if (adapter.getItem(i).getSymbol().equals(symbol))
            {
                return i;
            }
        }
        return -1;
    }

    private void addOnclickListenerToListView()
    {
        DynamicListView lstView =(DynamicListView) findViewById(R.id.main_activity_list_view);
        lstView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                TextView textView =(TextView)view.findViewById(R.id.FavLst_textViewSymbol);
                String symbol = textView.getText().toString();
                Intent intent = new Intent(M_Activity, ResultActivity.class);

                intent.putExtra("symbol", symbol);
                M_Activity.startActivity(intent);
            }

        });
    }


}
