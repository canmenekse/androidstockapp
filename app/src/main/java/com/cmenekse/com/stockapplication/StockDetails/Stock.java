package com.cmenekse.com.stockapplication.StockDetails;

/**
 * Created by canmenekse on 10/04/16.
 */
public class Stock
{

    private String Name;
    private String Symbol;
    private double LastPrice;
    private double Change;
    private double ChangePercent;
    private long MarketCap;
    private int Volume;
    private double ChangeYTD;
    private double ChangePercentYTD;
    private double High;
    private double Low;
    private double Open;
    private String Timestamp;



    private String timestampLocal;

    public Stock()
    {

    }

    public double getOpen()
    {
        return Open;
    }

    public void setOpen(double open)
    {
        this.Open = open;
    }

    public String getSymbol()
    {
        return Symbol;
    }

    public void setSymbol(String symbol)
    {
        this.Symbol = symbol;
    }

    public double getLastPrice()
    {
        return LastPrice;
    }

    public void setLastPrice(double lastPrice)
    {
        this.LastPrice = lastPrice;
    }

    public double getChange()
    {
        return Change;
    }

    public void setChange(double change)
    {
        this.Change = change;
    }

    public double getChangePercent()
    {
        return ChangePercent;
    }

    public void setChangePercent(double changePercent)
    {
        this.ChangePercent = changePercent;
    }

    public long getMarketCap()
    {
        return MarketCap;
    }

    public void setMarketCap(int marketCap)
    {
        this.MarketCap = marketCap;
    }

    public int getVolume()
    {
        return Volume;
    }

    public void setVolume(int volume)
    {
        this.Volume = volume;
    }

    public double getChangeYTD()
    {
        return ChangeYTD;
    }

    public void setChangeYTD(double changeYTD)
    {
        this.ChangeYTD = changeYTD;
    }

    public double getChangePercentYTD()
    {
        return ChangePercentYTD;
    }

    public void setChangePercentYTD(double changePercentYTD)
    {
        this.ChangePercentYTD = changePercentYTD;
    }

    public double getHigh()
    {
        return High;
    }

    public void setHigh(double high)
    {
        this.High = high;
    }

    public double getLow()
    {
        return Low;
    }

    public void setLow(double low)
    {
        this.Low = low;
    }

    public String getName()
    {
        return Name;
    }

    public void setName(String name)
    {
        this.Name = name;
    }

    public String getTimestampDefault()
    {
        return Timestamp;
    }

    public void setTimestampDefault(String timestampDefault)
    {
        this.Timestamp = timestampDefault;
    }

    public String getTimestampLocal()
    {
        return timestampLocal;
    }

    public void setTimestampLocal(String timestampLocal)
    {
        this.timestampLocal = timestampLocal;
    }

    public void setMarketCap(long marketCap)
    {
        MarketCap = marketCap;
    }







}