package com.cmenekse.com.stockapplication.Lookup;

/**
 * Created by canmenekse on 08/04/16.
 */
public class LookupWrapper
{

    private String Symbol;
    private String Name;
    private String Exchange;


    public LookupWrapper()
    {

    }

    public String getSymbol()
    {
        return Symbol;
    }

    public void setSymbol(String symbol)
    {
        this.Symbol = symbol;
    }

    public String getName()
    {
        return Name;
    }

    public void setName(String name)
    {
        this.Name = name;
    }


    public String getExchange()
    {
        return Exchange;
    }

    public void setExchange(String Exchange)
    {
        this.Exchange = Exchange;
    }
}
